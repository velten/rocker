# Image for https://indexplorer.embl.de/

## Building image
docker build -t git.embl.de:4567/velten/rocker/shiny:3.5.1-indeXplorer .

## Push it 
docker push git.embl.de:4567/velten/rocker/shiny:3.5.1-indeXplorer
